echo "This is the deploy step"

export PROJECT_ID=assign4jwr926
/assign4/google-cloud-sdk/bin/gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b



/assign4/google-cloud-sdk/bin/gcloud container clusters get-credentials testcluster1

kubectl delete deployment moviesweb4-deployment || echo "moviesweb4-deployment deployment does not exist"
kubectl delete service moviesweb4-deployment || echo "moviesweb4-deployment service does not exist"
kubectl delete ingress moviesweb4-ingress || echo "moviesweb4-ingress does not exist"

kubectl create -f deployment.yaml
kubectl expose deployment moviesweb4-deployment --target-port=5000 --type=NodePort

kubectl apply -f ingress.yaml

echo "Done deploying"
