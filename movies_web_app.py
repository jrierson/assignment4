import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = ('CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title TEXT NOT NULL, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, PRIMARY KEY (id))')

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        # try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        # except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


@app.route('/add_movie', methods=['POST'])
def add_movie():
    yr = int(request.form['year'])
    tl = request.form['title']
    direc = request.form['director']
    a = request.form['actor']
    rd = request.form['release_date']
    rate = float(request.form['rating'])


    if not yr:
        message = ('Movie %s could not be inserted - Year field is empty' % tl)
        return render_template('index.html', message=message)

    if not tl:
        message = 'Movie could not be inserted - Title Field is empty'
        return render_template('index.html', message=message)

    if not direc:
        message = ('Movie %s could not be inserted - Director Field is empty' % tl)
        return render_template('index.html', message=message)

    if not a:
        message = ('Movie %s could not be inserted - Actor Field is empty' % tl)
        return render_template('index.html', message=message)

    if not rd:
        message = ('Movie %s could not be inserted - Release date is empty' % tl)
        return render_template('index.html', message=message)

    if not rate:
        message = ('Movie %s could not be inserted - Rating is empty' % tl)
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("""SELECT title FROM movies""")
    emov = cur.fetchall()
    for x in emov:
        if x[0].upper() == tl.upper():
            message = ("Movie %s could not be inserted - Movie is already in database" % tl)
            return render_template('index.html', message=message)

    message = ("Movie %s successfully inserted" % tl)
    cur.execute("""INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)""", (yr, tl.upper(), direc, a.upper(), rd, rate))
    cnx.commit()

    return render_template('index.html', message=message)


@app.route('/update_movie', methods=['POST'])
def update_movie():
    yr = int(request.form['year'])
    tl = request.form['title']
    direc = request.form['director']
    a = request.form['actor']
    rd = request.form['release_date']
    rate = float(request.form['rating'])

    if not yr:
        message = ('Movie %s could not be updated - Year field is empty' % tl)
        return render_template('index.html', message=message)

    if not tl:
        message = 'Movie could not be updated - Title Field is empty'
        return render_template('index.html', message=message)

    if not direc:
        message = ('Movie %s could not be updated - Director Field is empty' % tl)
        return render_template('index.html', message=message)

    if not a:
        message = ('Movie %s could not be updated - Actor Field is empty' % tl)
        return render_template('index.html', message=message)

    if not rd:
        message = ('Movie %s could not be updated - Release date is empty' % tl)
        return render_template('index.html', message=message)

    if not rate:
        message = ('Movie %s could not be updated - Rating is empty' % tl)
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("""SELECT title FROM movies""")
    emov = cur.fetchall()

    titles = []

    for x in emov:
        titles.append(x[0])

    if tl.upper() not in titles:
        message = ('Movie %s could not be updated - It is not in the Database' % tl)
        return render_template('index.html', message=message)

    message = ("Movie %s successfully updated" % tl)
    cur.execute("""UPDATE movies SET year = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE title = %s""", (yr, direc, a.upper(), rd, rate, tl.upper()))
    cnx.commit()

    return render_template('index.html', message=message)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    tl = request.form['delete_title']

    if not tl:
        message = 'Movie could not be deleted - Title Field is empty'
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("""SELECT title FROM movies""")
    emov = cur.fetchall()

    titles = []

    for x in emov:
        titles.append(x[0])

    if tl.upper() not in titles:
        message = ('Movie with %s does not exist' % tl)
        return render_template('index.html', message=message)

    message = ("Movie %s successfully deleted" % tl)
    delsqlstate = "DELETE FROM movies WHERE title = %s"
    delete_title = (tl.upper(),)
    cur.execute(delsqlstate, delete_title)
    cnx.commit()

    return render_template('index.html', message=message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    a = request.args.get('search_actor')


    if not a:
        message = 'Actor Field cannot be empty'
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    sqlquery = "SELECT title, year, actor FROM movies WHERE actor = %s"
    actor = (a.upper(),)
    cur = cnx.cursor()
    cur.execute(sqlquery, actor)
    data = cur.fetchall()
    cnx.commit()
    if not data:
        message = ('No movies found for actor %s' % a)
        return render_template('index.html', message=message)
    else:
        return render_template('index.html', data=data)


@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sqlquery = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT max(rating) FROM movies)"
    cur.execute(sqlquery)
    #list = [dict(Title=row[0], Year=row[1], Actor=row[2], Director=row[3], Rating=row[4]) for row in cur.fetchall()]
    data = cur.fetchall()
    cnx.commit()
    return render_template('index.html', data=data)



@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sqlquery = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT min(rating) FROM movies)"
    cur.execute(sqlquery)
    #list = [dict(Title=row[0], Year=row[1], Actor=row[2], Director=row[3], Rating=row[4]) for row in cur.fetchall()]
    data = cur.fetchall()
    cnx.commit()
    return render_template('index.html', data=data)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/')
def index():
    return render_template('index.html')

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
